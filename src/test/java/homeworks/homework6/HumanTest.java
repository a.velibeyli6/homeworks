package homeworks.homework6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
    String[][] schedule = {{DayOfWeek.Monday.name(), "go to the courses"}, {DayOfWeek.Tuesday.name(), "do homeworks"}, {DayOfWeek.Wednesday.name(), "p"}, {DayOfWeek.Thursday.name(), "go to the courses"}, {DayOfWeek.Friday.name(), "go to the courses"}, {DayOfWeek.Saturday.name(), "go to the courses"}, {DayOfWeek.Sunday.name(), "go to the courses"}};
    Human human;

    @BeforeEach
    public void setUp() throws Exception {
        this.human = new Human("Alexander", "Pushkin", 2000, 80, schedule);
    }

    @Test
    void testToString() {
        String madeInMain = "Human{ name='Alexander', surname='Pushkin', year=2000, iq=80, schedule=[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]] }";
        assertEquals(madeInMain, human.toString());
    }
}