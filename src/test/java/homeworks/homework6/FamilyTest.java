package homeworks.homework6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    String[][] schedule = {{DayOfWeek.Monday.name(), "go to the courses"}, {DayOfWeek.Tuesday.name(), "do homeworks"}, {DayOfWeek.Wednesday.name(), "p"}, {DayOfWeek.Thursday.name(), "go to the courses"}, {DayOfWeek.Friday.name(), "go to the courses"}, {DayOfWeek.Saturday.name(), "go to the courses"}, {DayOfWeek.Sunday.name(), "go to the courses"}};
    Human mother = new Human("Anna", "Karenina", 1980, 90, schedule);
    Human father = new Human("Lev", "Tolstoy", 1976, 90, schedule);

    String habits[] = {"playing", "sleeping", "woofing", "lying on the ground"};
    Pet pet = new Pet(Species.Dog, "pet", 5, 100, habits);

    Family family;

    @BeforeEach
    public void setUp() throws Exception {
        this.family = new Family(mother, father);
        family.setPet(pet);
    }

    @Test
    void addChild() {
        boolean added = family.addChild(new Human("Alexander", "Pushkin", 2000));
        assertTrue(added);
    }

    @Test
    void deleteChildById() {
        Human child1 = new Human("Alexander", "Pushkin", 2000);
        Human child2 = new Human("Mikhail", "Lermontov", 2001);
        family.addChild(child1);
        family.addChild(child2);
        Human deletedChild = family.deleteChild(1);
        assertEquals(child2, deletedChild);
    }

    @Test
    void DeleteChildByObjext() {
        Human child1 = new Human("Alexander", "Pushkin", 2000);
        Human child2 = new Human("Mikhail", "Lermontov", 2001);
        family.addChild(child1);
        family.addChild(child2);
        boolean wasDeleted = family.deleteChild(child1);
        assertTrue(wasDeleted);
    }

    @Test
    void countFamily() {
        Human child1 = new Human("Alexander", "Pushkin", 2000);
        Human child2 = new Human("Mikhail", "Lermontov", 2001);
        family.addChild(child1);
        family.addChild(child2);
        int count = family.countFamily();
        assertEquals(4, count);
    }

    @Test
    void testToString() {
        Human child1 = new Human("Alexander", "Pushkin", 2000, 80, schedule);
        Human child2 = new Human("Mikhail", "Lermontov", 2001, 81, schedule);
        family.addChild(child1);
        family.addChild(child2);
        // I created in Main.java the family and copied its toString() method
        String toStringMadeInMain = "Father ==> name='Lev', surname='Tolstoy', year=1976, iq=90, schedule= {[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Mother ==> name='Anna', surname='Karenina', year=1980, iq=90, schedule= {[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Children ==> name='Alexander', surname='Pushkin', year=2000, iq=80, schudule=[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Children ==> name='Mikhail', surname='Lermontov', year=2001, iq=81, schudule=[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Pet ==> nickname='pet', species='Dog', age=5, tricklevel=100, habits=[playing, sleeping, woofing, lying on the ground]";

//        The reason I copied toString() here is that -> it gave me errors and I think that they had occurred because of Escape characters in the end of each line \n
        String familyToStringCopiedHere = "Father ==> name='Lev', surname='Tolstoy', year=1976, iq=90, schedule= {[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Mother ==> name='Anna', surname='Karenina', year=1980, iq=90, schedule= {[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Children ==> name='Alexander', surname='Pushkin', year=2000, iq=80, schudule=[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Children ==> name='Mikhail', surname='Lermontov', year=2001, iq=81, schudule=[[Monday, go to the courses], [Tuesday, do homeworks], [Wednesday, p], [Thursday, go to the courses], [Friday, go to the courses], [Saturday, go to the courses], [Sunday, go to the courses]]\n" +
                "Pet ==> nickname='pet', species='Dog', age=5, tricklevel=100, habits=[playing, sleeping, woofing, lying on the ground]";

        assertEquals(toStringMadeInMain,familyToStringCopiedHere);
    }
}