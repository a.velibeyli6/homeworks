package homeworks.homework6;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetTest {
    String habits[] = {"playing", "sleeping", "woofing", "lying on the ground"};
    Pet pet;

    @BeforeEach
    public void setUp() throws Exception {
        this.pet = new Pet(Species.Dog, "pet", 5, 100, habits);
    }

    @Test
    void testToString() {
        String madeInMain = "Dog{ nickname = 'pet', age = 5, tricklevel = 100, habits = [playing, sleeping, woofing, lying on the ground] }";
        assertEquals(madeInMain, pet.toString());
    }
}