package homeworks.homework9.service;

import homeworks.homework9.dao.CollectionFamilyDao;
import homeworks.homework9.dao.FamilyDao;
import homeworks.homework9.entities.Dog;
import homeworks.homework9.entities.Family;
import homeworks.homework9.entities.Human;
import homeworks.homework9.entities.Pet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    FamilyDao<Family> familyDao;
    Human mother1;
    Human father1;
    Human mother2;
    Human father2;
    Family family1;
    Family family2;
    Human child1;
    Pet dog;

    @BeforeEach
    void setUp() {
        familyDao = new CollectionFamilyDao();
        mother1 = new Human("Diana", "Keaton", 1970);
        father1 = new Human("Al", "Pacino", 1970);
        family1 = new Family(mother1, father1);

        mother2 = new Human("Jennifer", "Lawrence", 1970);
        father2 = new Human("Bradly", "Scott", 1970);
        family2 = new Family(mother2, father2);

        child1 = new Human("Antonio", "Banderas", 1999);

        dog = new Dog("Hasky", 2, 75);
    }

    @Test
    void getAllFamilies() {
        familyDao.saveFamily(new Family(mother1, father1));
        familyDao.saveFamily(new Family(mother2, father2));
        List<Family> allFamilies = familyDao.getAllFamilies();
        List<Family> hereFamilies = new ArrayList<>();
        hereFamilies.add(family1);
        hereFamilies.add(family2);
        assertEquals(hereFamilies, allFamilies);
    }

    @Test
    void displayAllFamilies() {
        StringBuilder actualSout = new StringBuilder();
        familyDao.saveFamily(new Family(mother1, father1));
        familyDao.saveFamily(new Family(mother2, father2));
        familyDao.getAllFamilies().forEach(f -> {
            actualSout.append(f.toString());
            actualSout.append("\n");
        });

        String predictedSout = "Father ==> name='Al', surname='Pacino', year=1970\n" +
                "Mother ==> name='Diana', surname='Keaton', year=1970\n" +
                "[]\n" +
                "Father ==> name='Bradly', surname='Scott', year=1970\n" +
                "Mother ==> name='Jennifer', surname='Lawrence', year=1970\n" +
                "[]\n";
        assertEquals(predictedSout, actualSout.toString());
    }

    @Test
    void getFamiliesBiggerThan() {
        familyDao.saveFamily(new Family(mother1, father1));
        familyDao.saveFamily(new Family(mother2, father2));
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        allFamilies.get(i).getChildren().add(child1);

        List<Family> expected = new ArrayList<>();
        expected.add(family1);
        int number = 2;
        List<Family> result = allFamilies.stream().filter(f -> (f.getChildren().size() + 2) > number).collect(Collectors.toList());

        assertEquals(expected, result);

    }

    @Test
    void getFamiliesLessThan() {
        familyDao.saveFamily(new Family(mother1, father1));
        familyDao.saveFamily(new Family(mother2, father2));
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        allFamilies.get(i).getChildren().add(child1);

        List<Family> expected = new ArrayList<>();
        expected.add(family2);
        int number = 3;
        List<Family> result = allFamilies.stream().filter(f -> (f.getChildren().size() + 2) < number).collect(Collectors.toList());

        assertEquals(expected, result);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        familyDao.saveFamily(new Family(mother1, father1));
        familyDao.saveFamily(new Family(mother2, father2));
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        allFamilies.get(i).getChildren().add(child1);

        List<Family> expected = new ArrayList<>();
        expected.add(family1);
        int number = 3;
        List<Family> result = allFamilies.stream().filter(f -> (f.getChildren().size() + 2) == number).collect(Collectors.toList());

        assertEquals(expected, result);
    }

    @Test
    void createNewFamily() {
        boolean b = familyDao.saveFamily(new Family(mother1, father1));
        assertTrue(b);
    }

    @Test
    void deleteFamilyByIndex() {
        familyDao.saveFamily(new Family(mother1, father1));
        boolean b = familyDao.deleteFamily(0);
        assertTrue(b);
    }

    @Test
    void bornChild() {
        familyDao.saveFamily(new Family(mother1, father1));
        String feminine = "Olqa";
        String masculine = "Oleq";
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        String name = (int) (Math.random() * 2) == 1 ? feminine : masculine;
        String surname = family1.getMother().getSurname();
        int year = LocalDate.now().getYear();
        Human child = new Human(name, surname, year);
        allFamilies.get(i).getChildren().add(child);

        assertEquals(3, allFamilies.get(i).getChildren().size() + 2);
    }

    @Test
    void adoptChild() {
        familyDao.saveFamily(new Family(mother1, father1));
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        allFamilies.get(i).getChildren().add(child1);
        assertEquals(3, allFamilies.get(i).getChildren().size() + 2);
    }

    @Test
    void deleteAllChildrenOlderThan() {
        familyDao.saveFamily(new Family(mother1, father1));
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        allFamilies.get(i).getChildren().add(child1);

        int number = 20;
        allFamilies.forEach(f -> f.getChildren().removeIf(child1 -> (LocalDate.now().getYear() - child1.getYear() > number)));
        assertEquals(2, allFamilies.get(i).getChildren().size() + 2);

    }

    @Test
    void count() {
        familyDao.saveFamily(new Family(mother1, father1));
        familyDao.saveFamily(new Family(mother2, father2));
        assertEquals(2, familyDao.getAllFamilies().size());
    }

    @Test
    void getFamilyById() {
        familyDao.saveFamily(new Family(mother1, father1));
        Family familyByIndex = familyDao.getFamilyByIndex(0);
        assertEquals(family1, familyByIndex);
    }

    @Test
    void getPets() {
        familyDao.saveFamily(new Family(mother1, father1));
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        allFamilies.get(i).getPet().add(dog);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        assertEquals(pets, allFamilies.get(i).getPet());
    }

    @Test
    void addPet() {
        familyDao.saveFamily(new Family(mother1, father1));
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family1);
        allFamilies.get(i).getPet().add(dog);
        assertFalse(allFamilies.get(i).getPet().size() == 0);
    }
}