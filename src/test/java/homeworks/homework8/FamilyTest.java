package homeworks.homework8;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


class FamilyTest {
//    HashMap<DayOfWeek, String> schedule;

    Human mother = new Human("Anna", "Karenina", 1980);
    Human father = new Human("Lev", "Tolstoy", 1976);

//    Set<String> habits;
//    Pet pet;

    Family family;

    @BeforeEach
    public void setUp() throws Exception {
//        this.schedule.put(DayOfWeek.Monday, "go to the courses");
//        this.schedule.put(DayOfWeek.Tuesday, "do homeworks");
//        this.schedule.put(DayOfWeek.Wednesday, "go to the courses");
//        this.schedule.put(DayOfWeek.Thursday, "go to the courses");
//        this.schedule.put(DayOfWeek.Friday, "go to the courses");
//        this.schedule.put(DayOfWeek.Saturday, "go to the courses");
//        this.schedule.put(DayOfWeek.Sunday, "go to the courses");

        this.family = new Family(mother, father);
//        this.habits.add("playing");
//        this.habits.add("sleeping");
//        this.habits.add("swimming");
//        this.habits.add("singing");
//        this.pet = new Fish("Petik", 22, 99, habits);
//        family.setPet(pet);
    }

    @Test
    void addChild() {
        boolean wasAdded = family.addChild(new Human("Alexander", "Kokorin", 1999));
        assertTrue(wasAdded);
    }

    @Test
    void deleteChildById() {
        Human child1 = new Human("Alexander", "Pushkin", 2000);
        Human child2 = new Human("Mikhail", "Lermontov", 2001);
        family.addChild(child1);
        family.addChild(child2);
        Human deletedChild = family.deleteChild(1);
        assertEquals(child2, deletedChild);
    }

    @Test
    void DeleteChildByObject() {
        Human child1 = new Human("Alexander", "Pushkin", 2000);
        Human child2 = new Human("Mikhail", "Lermontov", 2001);
        family.addChild(child1);
        family.addChild(child2);
        boolean wasDeleted = family.deleteChild(child1);
        assertTrue(wasDeleted);
    }

    @Test
    void countFamily() {
        Human child1 = new Human("Alexander", "Pushkin", 2000);
        Human child2 = new Human("Mikhail", "Lermontov", 2001);
        Human child3 = new Human("Maksim", "Qorkiy", 2002);
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        int count = family.countFamily();
        assertEquals(5, count);
    }

    @Test
    void testToString() {
        //cant do it;

    }
}