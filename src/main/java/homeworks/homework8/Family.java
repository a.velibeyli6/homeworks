package homeworks.homework8;

import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;
    Random random = new Random();

    /////////////////////////////////////////////////////////////////////////////////////////////////////

    public Human getMother() {
        return this.mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return this.father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return this.children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return this.pets;
    }

    public void setPet(Pet pet) {
        this.pets.add(pet);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public Family(Human mother, Human father) {// ADDING FAMILY ONLY WHEN INITIALIZE IT.
        this.mother = mother;
        this.father = father;
        children = new ArrayList<>();
        pets = new HashSet<>();
    }

    public boolean addChild(Human child) {
        return children.add(child);
    }

    public Human deleteChild(int index) {
        return children.remove(index);

    }

    public boolean deleteChild(Human o) {
        return children.remove(o);
    }

    public int countFamily() {
        return children.size() + 2;// Parents plus children;
    }


    @Override
    public void finalize() {
        System.out.println("I'm gonna die, wish me luck\nwith love <3 Family");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        StringBuilder show = new StringBuilder();
        show.append("Father ==> name='").append(this.father.getName()).append("', surname='").append(this.father.getSurname()).append("', year=").append(this.father.getYear()).append(", iq=").append(this.mother.getIq()).append(", schedule= {").append((this.mother.getSchedule().toString())).append("\n");
        show.append("Father ==> name='").append(this.father.getName()).append("', surname='").append(this.father.getSurname()).append("', year=").append(this.father.getYear()).append(", iq=").append(this.father.getIq()).append(", schedule= {").append((this.father.getSchedule().toString())).append("\n");
        for (Human child : children) {
            if (child != null) {
                show.append("Children ==> name='").append(child.getName()).append("', surname='").append(child.getSurname()).append("', year=").append(child.getYear()).append(", ");
                show.append("iq=").append(child.getIq()).append(", schudule=").append((child.getSchedule().toString())).append("\n");
            }
        }
        show.append(pets.toString());
        return show.toString();
    }


}