package homeworks.homework8;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.Monday, "go to the courses");
        schedule.put(DayOfWeek.Tuesday, "do homeworks");
        schedule.put(DayOfWeek.Wednesday, "go to the courses");
        schedule.put(DayOfWeek.Thursday, "go to the courses");
        schedule.put(DayOfWeek.Friday, "go to the courses");
        schedule.put(DayOfWeek.Saturday, "go to the courses");
        schedule.put(DayOfWeek.Sunday, "go to the courses");
        Human Phoebe = new Human("Phoebe", "Buffay", 1970, 75, schedule);
        Human Max = new Human("Max", "Buffay", 1971, 80, schedule);
        Human Joey = new Human("Joey", "Tribianni", 2000, 75, schedule);
        Human Rachel = new Human("Rachel", "Green", 2001, 78, schedule);
        Human Ross = new Human("Ross", "Geller", 1999, 85, schedule);
        Set<String> habits = new HashSet<>();
        habits.add("playing");
        habits.add("sleeping");
        habits.add("woofing");
        habits.add("lying on the ground");
        Pet pet = new Dog("pet", 5, 100, habits);
        Family friends = new Family(Phoebe, Max);
        friends.setPet(pet);
        friends.addChild(Joey);
        friends.addChild(Ross);
        friends.addChild(Rachel);

    }
}
