package homeworks.homework8;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;// 1-100
    private Set<String> habits;

    public Pet() {
        this.species = Species.UNKNOWN;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
        habits = new HashSet<>();
    }

    public Pet(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public String getNickname() {
        return this.nickname;
    }

    public Set<String> getHabits() {
        return this.habits;
    }

    public int getAge() {
        return this.age;
    }

    public Species getSpecies() {
        return this.species;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void eat() {
        System.out.println("I'm eating");
    }

    public abstract void respond();

    @Override
    public void finalize() {
        System.out.println("I'm gonna die, wish me luck\nwith love " + nickname);
    }


    @Override
    public String toString() {
        String show = this.species + "{ nickname = '" + this.nickname + "', age = " + this.age + ", tricklevel = " + this.trickLevel + ", habits = " + this.habits.toString() + " }";
        return show;
    }
}
