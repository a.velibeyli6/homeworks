package homeworks.homework8;

public enum Species {
    Dog,
    DomesticCat,
    RoboCat,
    Fish,
    Snake,
    Carrot,
    Mouse,
    Rat,
    Tortuga,
    UNKNOWN;
}
