package homeworks;
import java.util.Scanner;
import java.util.Random;
import java.lang.System;

public class Homework1 {
    public static void main(String[] args) {
        String[][] question=new String[3][2];
        question[0][0]="When did the WWI start?: ";
        question[0][1]="1914";
        question[1][0]="When did the WWII start?: ";
        question[1][1]="1939";
        question[2][0]="When did the Manchester United win CL last time?: ";
        question[2][1]="2008";
        Scanner in = new Scanner(System.in);
        System.out.println("What is your name?");
        String name = in.nextLine();
        System.out.println("Let the game begin!");
        Random random =new Random();
        int secret=random.nextInt(3);
//        int secret = random.nextInt(100+1);
        while(true){
            System.out.print(question[secret][0]);
            int guess = in.nextInt();
            if(guess==Integer.parseInt(question[secret][1])){
                System.out.printf("Congratulations, %s",name);
                break;
            }else if(guess>Integer.parseInt(question[secret][1])){
                System.out.println("Your number is too big. Please, try again.");
            }else{
                System.out.println("Your number is too small. Please, try again.");
            }
        }
    }
}