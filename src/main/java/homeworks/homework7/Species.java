package homeworks.homework7;

import org.omg.CORBA.UNKNOWN;

public enum Species {
    Dog,
    DomesticCat,
    RoboCat,
    Fish,
    Snake,
    Carrot,
    Mouse,
    Rat,
    Tortuga,
    UNKNOWN;
}
