package homeworks.homework7;


public class Fish extends Pet {

    public Fish(String nickname, int age, int trickLevel, String[] habits) {
        super(Species.Fish,nickname,age,trickLevel,habits);
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am - " + getNickname() + ". I miss you!");
    }
}