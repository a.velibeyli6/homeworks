package homeworks.homework7;

import java.util.Set;

public class Dog extends Pet implements Foulable {

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(Species.Dog,nickname,age,trickLevel,habits);
    }


    @Override
    public void respond() {
        System.out.println("Hello, owner. I am - " + getNickname() + ". I miss you!");
    }

    @Override
    public void foul() {
        System.out.println("I need to cover it up");
    }
}

