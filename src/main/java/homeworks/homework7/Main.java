package homeworks.homework7;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = {{DayOfWeek.Monday.name(), "go to the courses"}, {DayOfWeek.Tuesday.name(), "do homeworks"}, {DayOfWeek.Wednesday.name(), "p"}, {DayOfWeek.Thursday.name(), "go to the courses"}, {DayOfWeek.Friday.name(), "go to the courses"}, {DayOfWeek.Saturday.name(), "go to the courses"}, {DayOfWeek.Sunday.name(), "go to the courses"}};
        Human Phoebe = new Human("Phoebe", "Buffay", 1970, 75, schedule);
        Human Max = new Human("Max", "Buffay", 1971, 80, schedule);
        Human Joey = new Human("Joey", "Tribianni", 2000, 75, schedule);
        Human Rachel = new Human("Rachel", "Green", 2001, 78, schedule);
        Human Ross = new Human("Ross", "Geller", 1999, 85, schedule);
        String habits[] = {"playing", "sleeping", "woofing", "lying on the ground"};
        Pet pet = new Dog("pet", 5, 100, habits);
        Family friends = new Family(Phoebe, Max);
        friends.setPet(pet);
        friends.addChild(Joey);
        friends.addChild(Ross);
        friends.addChild(Rachel);

    }
}
