package homeworks.homework11.entities;

public class RoboCat extends Pet {

    public RoboCat(String nickname, int age, int trickLevel) {
        super(Species.RoboCat,nickname,age,trickLevel);
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am - " + getNickname() + ". I miss you!");
    }
}
