package homeworks.homework11.entities;

import homeworks.homework8.Family;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Human {
    private String name, surname;
    private long birthDate;
    private Family family;
    private int iq;
    private String dateOfBirth; // 20/03/2016

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public long getBirthDate() {
        return this.birthDate;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////


    public Human() {
    }

    public Human(String name, String surname, long birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public Human(String name, String surname, String dateOfbirth, int iq) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfbirth;
        this.iq = iq;
    }


    public String describeAge() {
        long days = birthDate / (1000 * 60 * 60 * 24);
        int years = (int) (days / 365);
        int months = (int) ((days - years * 365) / 30);
        int day = (int) ((days - years * 365 - months * 30));
        return years + " years, " + months + " months, " + day + " days";
    }


    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Human)) return false;
        Human that = (Human) o;
        if (this.birthDate != that.birthDate) return false;
        if (!this.name.equals(that.name)) return false;
        if (!this.surname.equals(that.surname)) return false;
        return true;
    }

    @Override
    public String toString() {
        String show;
        Date date = new Date(birthDate);

        String line = new SimpleDateFormat("dd/MM/yyyy").format(date);
        show = "Human{ name='" + this.name + "', surname='" + this.surname + "', year=" + line + " }";
        return show;
    }
}
