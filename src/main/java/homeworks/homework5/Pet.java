package homeworks.homework5;

import java.util.Arrays;

public class Pet {

    private String species;
    private String nickname;
    private int age;
    private int trickLevel;// 1-100
    private String[] habits;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getSpecies() {
        return this.species;
    }

    public String[] getHabits() {
        return this.habits;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void eat() {
        System.out.println("I'm eating");
    }

    public void respond() {
        System.out.println("Hello, owner. I am - " + this.nickname + ". I miss you!");
    }

    public void foul() {
        System.out.println("I need to cover it up");
    }

    public Pet() {
    }

    public Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this(species,nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }
    @Override
    public String toString() {
        String show = this.species + "{ nickname = '" + this.nickname + "', age = " + this.age + ", tricklevel = " + this.trickLevel + ", habits = " + Arrays.toString(this.habits) + " }";
        return show;
    }
}
