package homeworks.homework5;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = {{"Monday", "go to the courses"}, {"Tueday", "do homeworks"}, {"Wednesday", "p"}, {"Thursday", "go to the courses"}, {"Friday", "go to the courses"}, {"Saturday", "go to the courses"}, {"Sunday", "go to the courses"}};
        Human Phoebe = new Human("Phoebe", "Buffay", 1970, 75, schedule);
        Human Max = new Human("Max", "Buffay", 1971, 80, schedule);
        Human Joey = new Human("Joey", "Tribianni", 2000, 75, schedule);
        Human Rachel = new Human("Rachel", "Green", 2001, 78, schedule);
        Human Ross = new Human("Ross", "Geller", 1999, 85, schedule);
        String habits[] = {"playing", "sleeping", "woofing", "lying on the ground"};
        Pet pet = new Pet("dog", "pet", 5, 100, habits);
        Family friends = new Family(Phoebe, Max);
        friends.setPet(pet);
        friends.addChild(Joey);
        friends.addChild(Ross);
        friends.addChild(Rachel);
        System.out.println(friends);
        System.out.println("in our family there are " + friends.countFamily() + " of us");
//        friends.deleteChild(1);// Ross got married again. it is his fifth wedding.
        friends.deleteChild(Joey);
        System.out.println(friends);
        System.out.println("in our family there are " + friends.countFamily() + " of us");
        friends.describePet();
        friends.greetPet();

    }
}
