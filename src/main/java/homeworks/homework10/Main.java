package homeworks.homework10;


import homeworks.homework10.controller.FamilyController;
import homeworks.homework10.entities.Dog;
import homeworks.homework10.entities.Family;
import homeworks.homework10.entities.Human;
import homeworks.homework10.entities.Pet;

import java.util.Date;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
//        FamilyController familyController = new FamilyController();
//
//        Pet dog = new Dog("Hasky", 2, 75);
//
//        Human mother = new Human("Jeniffer", "Aniston", 1972, 70);
//        Human father = new Human("Brad", "Pitt", 1970, 70);
//        Family family1 = new Family(mother, father);
//        familyController.createNewFamily(mother, father);
//        familyController.addPet(family1, dog); // AD PET
//        System.out.println("These are 1 family's pets");
//        Set<Pet> pets = familyController.getPets(0); // GET PETS
//        pets.forEach(System.out::println);
//        System.out.println(familyController.count());
//        Human father2 = new Human("John", "Wick", 1970, 70);
//        Human mother2 = new Human("Alice", "Weber", 1967, 70);
//        Family family2 = new Family(mother2, father2);
//        familyController.createNewFamily(mother2, father2); // CREATE NEW FAMILY
//        familyController.bornChild(family1, "Kate", "Darren");
//        System.out.println("========================================================================");
//        familyController.displayAllFamilies();
//        System.out.println("========================================================================");
//
//        familyController.bornChild(family1, "Lisa", "Davis"); // BORN CHILD
//        familyController.bornChild(family1, "Diana", "Donald");
//        familyController.bornChild(family2, "Sofia", "Michael");
//        familyController.bornChild(family2, "Monica", "Wayne");
//        familyController.bornChild(family2, "Angelina", "George");
//        Human child = new Human("Selena", "Davidson", 1995, 70);
//        familyController.adoptChild(family1, child); // ADOPT CHILD
//        System.out.println("========================================================================");
//        familyController.displayAllFamilies();
//        System.out.println("========================================================================");
//        familyController.getAllFamilies().forEach(f -> System.out.println(f.getChildren().size() + 2));
//        int i = familyController.countFamiliesWithMemberNumber(5); // COUNT FAMILIES WITH MEMBER NUMBER
//        System.out.println("there are " + i + " families with 5 members");
//        familyController.getFamiliesBiggerThan(3); // GET FAMILIES BIGGER THAN
//        familyController.getFamiliesLessThan(6); // GET FAMILIES LESS THAN
//        Family familyById = familyController.getFamilyById(1); // GET FAMILY BY ID
//        System.out.println(familyById);
//        familyController.deleteFamilyByIndex(1); // DELETE FAMILY BY INDEX
//        System.out.println("========================================================================");
//        familyController.displayAllFamilies();
//        System.out.println("========================================================================");
//        familyController.deleteAllChildrenOlderThan(20);  // DELETE ALL CHILDREN OLDER THAN
//        System.out.println("========================================================================");
//        familyController.displayAllFamilies();
//        System.out.println("========================================================================");
        Human child = new Human("Selena", "Walt", "21/02/1995", 86);
        Human mother = new Human("Jeniffer", "Aniston", 999999999, 70);
        System.out.println(mother);


    }
}
