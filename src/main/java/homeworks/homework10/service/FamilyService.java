package homeworks.homework10.service;


import homeworks.homework10.dao.CollectionFamilyDao;
import homeworks.homework10.dao.FamilyDao;
import homeworks.homework10.entities.Family;
import homeworks.homework10.entities.Human;
import homeworks.homework10.entities.Pet;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    FamilyDao<Family> familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = familyDao.getAllFamilies();
        allFamilies.forEach(System.out::println);
    }

    public void getFamiliesBiggerThan(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) > number).collect(Collectors.toList());
        collected.forEach(System.out::println);
    }

    public void getFamiliesLessThan(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) < number).collect(Collectors.toList());
        collected.forEach(System.out::println);
    }

    public int countFamiliesWithMemberNumber(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) == number).collect(Collectors.toList());
        return collected.size();
    }

    public void createNewFamily(Human mother, Human father) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String feminine, String masculine) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        String name = (int) (Math.random() * 2) == 1 ? feminine : masculine;
        String surname = family.getMother().getSurname();
        int year = LocalDate.now().getYear();
        Human child = new Human(name, surname, year, 0);
        allFamilies.get(i).getChildren().add(child);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        allFamilies.get(i).getChildren().add(child);

        return family;
    }


    public void deleteAllChildrenOlderThan(int number) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        allFamilies.forEach(f -> {
            f.getChildren().removeIf(child -> (LocalDate.now().getYear() - (int) ((child.getBirthDate()) / (1000 * 60 * 60 * 24 * 365))) > number);
        });

    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        Family familyByIndex = familyDao.getFamilyByIndex(index);
        return familyByIndex.getPet();
    }

    public void addPet(Family family, Pet pet) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        allFamilies.get(i).getPet().add(pet);
    }
}
