package homeworks.homework10.entities;

public final class Man extends Human {

    public void greetPet() {
        System.out.println("Hello, my pet, how are you? ");
    }
    public void repairCar(){
        System.out.println("Now I'm gonna repair my car");
    }
}
