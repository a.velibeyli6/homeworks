package homeworks.homework10.entities;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;

    public Pet() {
        this.species = Species.UNKNOWN;
    }

    public Pet(Species species, String nickname, int age, int trickLevel) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public Species getSpecies() {
        return this.species;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void eat() {
        System.out.println("I'm eating");
    }

    public abstract void respond();

    @Override
    public void finalize() {
        System.out.println("I'm gonna die, wish me luck\nwith love " + nickname);
    }


    @Override
    public String toString() {
        String show = this.species + "{ nickname = '" + this.nickname + "', age = " + this.age + ", tricklevel = " + this.trickLevel + " }";
        return show;
    }
}
