package homeworks.homework10.entities;

public class Dog extends Pet implements Foulable {

    public Dog(String nickname, int age, int trickLevel) {
        super(Species.Dog,nickname,age,trickLevel);
    }


    @Override
    public void respond() {
        System.out.println("Hello, owner. I am - " + getNickname() + ". I miss you!");
    }

    @Override
    public void foul() {
        System.out.println("I need to cover it up");
    }
}
