package homeworks.homework10.dao;


import homeworks.homework10.entities.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao<Family> {

    List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        int count = families.size();
        families.remove(index);
        int count2 = families.size();
        return count != count2;
    }

    @Override
    public boolean deleteFamily(Family f) {
        return families.remove(f);
    }

    @Override
    public boolean saveFamily(Family f) {
        families.add(f);
        return true;
    }
}
