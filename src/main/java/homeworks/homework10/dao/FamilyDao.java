package homeworks.homework10.dao;



import java.util.List;

public interface FamilyDao<A> {
    List<A> getAllFamilies();
    A getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    boolean deleteFamily(A f);
    boolean saveFamily(A f);

}
