package homeworks.homework9.entities;

import homeworks.homework8.DayOfWeek;
import homeworks.homework8.Family;

import java.util.Map;

public class Human {
    private String name, surname;
    private int year;
    private Family family;

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public int getYear() {
        return this.year;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////


    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (!(o instanceof Human)) return false;
        Human that = (Human) o;
        if (this.year != that.year) return false;
        if (!this.name.equals(that.name)) return false;
        if (!this.surname.equals(that.surname)) return false;
        return true;
    }

    @Override
    public String toString() {
        String show;
        show = "Human{ name='" + this.name + "', surname='" + this.surname + "', year=" + this.year + " }";
        return show;
    }
}
