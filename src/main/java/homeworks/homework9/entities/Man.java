package homeworks.homework9.entities;

import homeworks.homework9.entities.Human;

public final class Man extends Human {

    public void greetPet() {
        System.out.println("Hello, my pet, how are you? ");
    }
    public void repairCar(){
        System.out.println("Now I'm gonna repair my car");
    }
}
