package homeworks.homework9.entities;

import homeworks.homework9.entities.Human;

public final class Woman extends Human {

    public void greetPet() {
        System.out.println("Hello, my pet, how are you? ");
    }
    public void makeUp(){
        System.out.println("Now I'm gonna make myself up");
    }

}
