package homeworks.homework9.entities;

import homeworks.homework9.entities.Pet;
import homeworks.homework9.entities.Species;

import java.util.Set;

public class Fish extends Pet {

    public Fish(String nickname, int age, int trickLevel) {
        super(Species.Fish,nickname,age,trickLevel);
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am - " + getNickname() + ". I miss you!");
    }
}
