package homeworks.homework9.entities;

import homeworks.homework9.dao.Identifialble;

import java.io.Serializable;
import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;
    Random random = new Random();


    /////////////////////////////////////////////////////////////////////////////////////////////////////

    public Human getMother() {
        return this.mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return this.father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return this.children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return this.pets;
    }

    public void setPet(Pet pet) {
        this.pets.add(pet);
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public Family(Human mother, Human father) {// ADDING FAMILY ONLY WHEN INITIALIZE IT.
        this.mother = mother;
        this.father = father;
        children = new ArrayList<>();
        pets = new HashSet<>();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return false;
        if (!(obj instanceof Family)) return false;

        Family that = (Family) obj;
//        if(that.getId() != this.getId()) return false;
        if(!(that.getMother().equals(this.getMother()))) return false;
        if(!(that.getFather().equals(this.getFather()))) return false;

        return true;
    }

    @Override
    public String toString() {
        StringBuilder show = new StringBuilder();
        show.append("Father ==> name='").append(this.father.getName()).append("', surname='").append(this.father.getSurname()).append("', year=").append(this.father.getYear()).append("\n");
        show.append("Mother ==> name='").append(this.mother.getName()).append("', surname='").append(this.mother.getSurname()).append("', year=").append(this.mother.getYear()).append("\n");
        for (Human child : children) {
                show.append("Children ==> name='").append(child.getName()).append("', surname='").append(child.getSurname()).append("', year=").append(child.getYear()).append("\n");
        }
        show.append(pets.toString());
        return show.toString();
    }


}