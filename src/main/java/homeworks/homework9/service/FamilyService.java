package homeworks.homework9.service;


import homeworks.homework9.dao.CollectionFamilyDao;
import homeworks.homework9.dao.FamilyDao;
import homeworks.homework9.entities.Family;
import homeworks.homework9.entities.Human;
import homeworks.homework9.entities.Pet;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    FamilyDao<Family> familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = familyDao.getAllFamilies();
        allFamilies.forEach(System.out::println);
    }

    public void getFamiliesBiggerThan(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) > number).collect(Collectors.toList());
        collected.forEach(System.out::println);
    }

    public void getFamiliesLessThan(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) < number).collect(Collectors.toList());
        collected.forEach(System.out::println);
    }

    public int countFamiliesWithMemberNumber(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) == number).collect(Collectors.toList());
        return collected.size();
    }

    public void createNewFamily(Human mother, Human father) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String feminine, String masculine) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        String name = (int) (Math.random() * 2) == 1 ? feminine : masculine;
        String surname = family.getMother().getSurname();
        int year = LocalDate.now().getYear();
        Human child = new Human(name, surname, year);
        allFamilies.get(i).getChildren().add(child);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        allFamilies.get(i).getChildren().add(child);

        return family;
    }


    public void deleteAllChildrenOlderThan(int number) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        allFamilies.forEach(f -> {
            f.getChildren().removeIf(child -> (LocalDate.now().getYear() - child.getYear()) > number);
        });

    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        Family familyByIndex = familyDao.getFamilyByIndex(index);
        return familyByIndex.getPet();
    }

    public void addPet(Family family, Pet pet) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        allFamilies.get(i).getPet().add(pet);
    }
}
