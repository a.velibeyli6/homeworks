package homeworks.homework9.dao;



import java.util.List;
import java.util.Optional;

public interface FamilyDao<A> {
    List<A> getAllFamilies();
    A getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    boolean deleteFamily(A f);
    boolean saveFamily(A f);

}
