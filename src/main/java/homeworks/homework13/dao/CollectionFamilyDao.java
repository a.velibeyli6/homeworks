package homeworks.homework13.dao;


import homeworks.homework13.entities.Family;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao<Family> {

    private final String file;

    public CollectionFamilyDao(String file) {
        this.file = file;
    }

    @Override
    public List<Family> getAllFamilies() {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            Object readed = ois.readObject();
            @SuppressWarnings("unchecked")
            List<Family> as = (ArrayList<Family>) readed;
            return as;
        } catch (ClassNotFoundException ex) {
            throw new RuntimeException("Deserialization error. Didn't you forget to include 'serialVersionUID field' in your entity?", ex);
        } catch (FileNotFoundException ex) {
            return new ArrayList<>();
        } catch (IOException ex) {
            throw new RuntimeException("Something went wrong", ex);
        }
    }

    @Override
    public Family getFamilyByIndex(int index) {
        int count = 0;
        for (Family f : getAllFamilies()) {
            if (count == index) return f;
            count++;
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        List<Family> allFamilies = getAllFamilies();
        allFamilies.remove(index);
        return write(allFamilies);
    }

    @Override
    public boolean deleteFamily(Family f) {
        List<Family> allFamilies = getAllFamilies();
        allFamilies.remove(f);
        return write(allFamilies);
    }

    @Override
    public boolean saveFamily(Family f) {
        List<Family> allFamilies = getAllFamilies();
        allFamilies.add(f);
        return write(allFamilies);
    }

    private boolean write(List<Family> allFamilies) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            oos.writeObject(allFamilies);
            return true;
        } catch (IOException ex) {
            throw new RuntimeException("DAO:write:IOException", ex);
        }
    }
}
