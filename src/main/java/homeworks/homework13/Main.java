package homeworks.homework13;


import homeworks.homework13.controller.FamilyController;
import homeworks.homework13.entities.Family;
import homeworks.homework13.entities.Human;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();

        StringBuilder print = new StringBuilder();
        print.append("+-------------------------------------------------------------------------------------------------+\n");
        print.append("+-------------------------------------------------------------------------------------------------+\n");
        print.append("| 1. Add new  data to the file                                                                    |\n");
        print.append("| 2. Display the entire list of families                                                          |\n");
        print.append("| 3. Display a list of families where the number of people is greater than the specified number   |\n");
        print.append("| 4. Display a list of families where the number of people is less than the specified number      |\n");
        print.append("| 5. Calculate the number of families where the number of members is                              |\n");
        print.append("| 6. Create a new family                                                                          |\n");
        print.append("| 7. Delete a family by its index in the general list                                             |\n");
        print.append("| 8. Edit a family by its index in the general list                                               |\n");
        print.append("| 9. Remove all children over the age of majority                                                 |\n");
        print.append("| 10. TO EXIT                                                                                     |\n");
        print.append("| 11. Load the data                                                                               |\n");
        print.append("+-------------------------------------------------------------------------------------------------+\n");
        print.append("|SELECT MENU (1-6) ->                                                                             |\n");
        print.append("+-------------------------------------------------------------------------------------------------+\n");

        int option;
        Scanner in = new Scanner(System.in);
        StringBuilder print2 = new StringBuilder();
        print2.append("1 - Give birth to a baby").append("\n");
        print2.append("2 - Adopt a child").append("\n");
        print2.append("3 - Return to main menu").append("\n");

        System.out.println(print.toString());
        try {
            option = in.nextInt();
            while (option != 10) {
                switch (option) {
                    case 1:
                        familyController.createRandomly();
                        break;
                    case 2:
                        familyController.displayAllFamilies();
                        break;
                    case 3:
                        System.out.println("Enter the number");
                        familyController.getFamiliesBiggerThan(in.nextInt());
                        break;
                    case 4:
                        System.out.println("Enter the number");
                        familyController.getFamiliesLessThan(in.nextInt());
                        break;
                    case 5:
                        System.out.println("Enter the number");
                        familyController.countFamiliesWithMemberNumber(in.nextInt());
                        break;
                    case 6:
                        System.out.println("Enter the mother's name");
                        String mname = in.nextLine();
                        System.out.println("Enter the mother's surname");
                        String msurname = in.nextLine();
                        System.out.println("Enter the mother's iq");
                        int miq = in.nextInt();
                        in.nextLine();
                        System.out.println("Enter the mother's birth year, month, day");
                        String myear = in.nextLine();
                        String mmonth = in.nextLine();
                        String mday = in.nextLine();
                        StringBuilder mdate = new StringBuilder();
                        mdate.append(mday).append("/").append(mmonth).append("/").append(myear);
                        Human mother = new Human(mname, msurname, mdate.toString(), miq);

                        System.out.println("Enter the father's name");
                        String fname = in.nextLine();
                        System.out.println("Enter the father's surname");
                        String fsurname = in.nextLine();
                        System.out.println("Enter the father's iq");
                        int fiq = in.nextInt();
                        in.nextLine();
                        System.out.println("Enter the father's birth year, month, day");
                        String fyear = in.nextLine();
                        String fmonth = in.nextLine();
                        String fday = in.nextLine();
                        StringBuilder fdate = new StringBuilder();
                        mdate.append(mday).append("/").append(mmonth).append("/").append(myear);
                        Human father = new Human(fname, fsurname, fdate.toString(), fiq);

                        familyController.createNewFamily(mother, father);
                        break;
                    case 7:
                        System.out.println("Enter the ID of the family");
                        familyController.deleteFamilyByIndex(in.nextInt());
                        break;
                    case 8:
                        System.out.println(print2.toString());
                        option = in.nextInt();
                        switch (option) {
                            case 1:
                                System.out.println("Enter the ID of the family");
                                Family familyById = familyController.getFamilyById(in.nextInt());
                                in.nextLine();
                                System.out.println("Enter the names you wish for children");
                                String man = in.nextLine();
                                String woman = in.nextLine();
                                familyController.bornChild(familyById, woman, man);
                                break;
                            case 2:
                                System.out.println("Enter the ID of the family");
                                Family familyById2 = familyController.getFamilyById(in.nextInt());
                                in.nextLine();
                                System.out.println("Enter the name, surname,year of birth, iq of the child ");
                                String name = in.nextLine();
                                String surname = in.nextLine();
                                String year = in.nextLine();
                                int iq = in.nextInt();
                                familyController.adoptChild(familyById2, new Human(name, surname, year, iq));
                                break;
                        }
                        break;
                    case 9:
                        System.out.println("Enter the age");
                        familyController.deleteAllChildrenOlderThan(in.nextInt());
                        break;
                    case 11:
                        System.out.println("We take a List<Family> as a parameter");
                        familyController.loadData(familyController.getAllFamilies());
                        break;
                }
                System.out.println(print.toString());
                option = in.nextInt();
            }
        } catch(InputMismatchException ex) {
            throw new InputMismatchException("Bad input");
        }
    }
}
