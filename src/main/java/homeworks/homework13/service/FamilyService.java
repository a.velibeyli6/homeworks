package homeworks.homework13.service;

import homeworks.homework13.dao.CollectionFamilyDao;
import homeworks.homework13.dao.FamilyDao;
import homeworks.homework13.entities.Family;
import homeworks.homework13.entities.Human;
import homeworks.homework13.entities.Pet;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    FamilyDao<Family> familyDao = new CollectionFamilyDao("families.bin");

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        List<Family> allFamilies = familyDao.getAllFamilies(); // Java 8 already
        allFamilies.forEach(f -> System.out.println(f.prettyFormat()));
    }

    public void getFamiliesBiggerThan(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) > number).collect(Collectors.toList()); // Java 8 already
        collected.forEach(System.out::println);
    }

    public void getFamiliesLessThan(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) < number).collect(Collectors.toList()); // Java 8 already
        collected.forEach(System.out::println);
    }

    public int countFamiliesWithMemberNumber(int number) {
        List<Family> collected = familyDao.getAllFamilies().stream().filter(f -> (f.getChildren().size() + 2) == number).collect(Collectors.toList()); // Java 8 already
        return collected.size();
    }

    public void createNewFamily(Human mother, Human father) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String feminine, String masculine) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        String name = (int) (Math.random() * 2) == 1 ? feminine : masculine;
        String surname = family.getMother().getSurname();
        int year = LocalDate.now().getYear();
        Human child = new Human(name, surname, year, 0);
        allFamilies.get(i).getChildren().add(child);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        allFamilies.get(i).getChildren().add(child);

        return family;
    }

    public void deleteAllChildrenOlderThan(int number) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        allFamilies.forEach(f -> {
            f.getChildren().removeIf(child -> (LocalDate.now().getYear() - (int) ((child.getBirthDate()) / (1000 * 60 * 60 * 24 * 365))) > number); // Java 8 already
        });

    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        Family familyByIndex = familyDao.getFamilyByIndex(index);
        return familyByIndex.getPet();
    }

    public void addPet(Family family, Pet pet) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        int i = allFamilies.indexOf(family);
        allFamilies.get(i).getPet().add(pet);
    }

    public void createRandomly() {
        List<String> manNames = new ArrayList<>();
        manNames.add("Alexander");
        manNames.add("Ivan");
        manNames.add("Daren");
        manNames.add("Willem");
        manNames.add("Donald");
        manNames.add("Ryan");
        manNames.add("Oleq");
        manNames.add("Brad");
        List<String> womanNames = new ArrayList<>();
        womanNames.add("Diana");
        womanNames.add("Olqa");
        womanNames.add("Lily");
        womanNames.add("Sarah");
        womanNames.add("Anastasiya");
        womanNames.add("Tatiana");
        womanNames.add("Elizabeth");
        womanNames.add("Kamila");
        List<String> surnames = new ArrayList<>();
        surnames.add("Davidson");
        surnames.add("Walt");
        surnames.add("Jackson");
        surnames.add("McCartney");
        surnames.add("Thompson");
        surnames.add("Williams");
        surnames.add("Jenkins");
        surnames.add("Colbert");
        surnames.add("Donnaldson");
        surnames.add("Neuer");
        surnames.add("Mueller");
        surnames.add("Corden");
        surnames.add("Wellington");
        surnames.add("Lawrence");
        surnames.add("Nogatomo");
        surnames.add("Osaka");
        int day[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
        int month[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        String[] year = {"1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977"};
        for (int i = 0; i < 15; i++) {
            String mname = manNames.get((int) (Math.random() * manNames.size()));
            String msurname = surnames.get((int) (Math.random() * surnames.size()));
            String mday = String.valueOf(day[(int) (Math.random() * day.length)]);
            String mmonth = String.valueOf(month[(int) (Math.random() * month.length)]);
            String myear = year[(int) (Math.random() * year.length)];
            int miq = 100 - Integer.parseInt(mmonth);
            StringBuilder mdate = new StringBuilder();
            mdate.append(mday).append("/").append(mmonth).append("/").append(myear);

            String wname = womanNames.get((int) (Math.random() * womanNames.size()));
            String wsurname = surnames.get((int) (Math.random() * surnames.size()));
            String wday = String.valueOf(day[(int) (Math.random() * day.length)]);
            String wmonth = String.valueOf(month[(int) (Math.random() * month.length)]);
            String wyear = year[(int) (Math.random() * year.length)];
            int wiq = 100 - Integer.parseInt(mmonth);
            StringBuilder wdate = new StringBuilder();
            wdate.append(wday).append("/").append(wmonth).append("/").append(wyear);

            Human father = new Human(mname, msurname, mdate.toString(), miq);
            Human mother = new Human(wname, wsurname, wdate.toString(), wiq);
            familyDao.saveFamily(new Family(mother, father));
        }


    }

    public void loadData(List<Family> allFamilies) {
        for (Family f : allFamilies) {
            familyDao.saveFamily(f);
        }
    }
}
