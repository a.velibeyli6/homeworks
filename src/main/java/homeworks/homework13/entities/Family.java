package homeworks.homework13.entities;

import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;
    Random random = new Random();
    private final int id;
    private static int ids = 0;


    /////////////////////////////////////////////////////////////////////////////////////////////////////

    public Human getMother() {
        return this.mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return this.father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return this.children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return this.pets;
    }

    public void setPet(Pet pet) {
        this.pets.add(pet);
    }

    public int getId() {
        return id;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public Family(Human mother, Human father) {// ADDING FAMILY ONLY WHEN INITIALIZE IT.
        this.mother = mother;
        this.father = father;
        children = new ArrayList<>();
        pets = new HashSet<>();
        id = ++ids;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof Family)) return false;

        Family that = (Family) obj;
//        if(that.getId() != this.getId()) return false;
        if (!(that.getMother().equals(this.getMother()))) return false;
        if (!(that.getFather().equals(this.getFather()))) return false;

        return true;
    }

    public String prettyFormat() {
        StringBuilder show = new StringBuilder();
        show.append("Father ==> ").append(father.prettyFormat()).append("\n");
        show.append("Mother ==> ").append(mother.prettyFormat()).append("\n");
        show.append("Children :").append("\n");
        for (Human child : children) {
            show.append(child.prettyFormat()).append("\n");
        }
        show.append("Pets :");
        for (Pet pet : pets) {
            show.append(pet.prettyFormat()).append("\n");
        }
        return show.toString();
    }


}