package homeworks.homework13.controller;

import homeworks.homework13.MyException;
import homeworks.homework13.entities.Family;
import homeworks.homework13.entities.Human;
import homeworks.homework13.entities.Pet;
import homeworks.homework13.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int number) {
        familyService.getFamiliesBiggerThan(number);
    }

    public void getFamiliesLessThan(int number) {
        familyService.getFamiliesLessThan(number);
    }

    public int countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String feminine, String masculine) throws MyException {
        if(family.getChildren().size()>7) throw new MyException("Too many children");
        else {
            return familyService.bornChild(family, feminine, masculine);
        }
    }

    public Family adoptChild(Family family, Human child) {
        if(family.getChildren().size()>7) throw new MyException("Too many children");
        else {
            return familyService.adoptChild(family, child);
        }
    }

    public void deleteAllChildrenOlderThan(int number) {
        familyService.deleteAllChildrenOlderThan(number);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(Family family, Pet pet) {
        familyService.addPet(family, pet);
    }

    public void createRandomly() {
        familyService.createRandomly();
    }

    public void loadData(List<Family> allFamilies) {
        familyService.loadData(familyService.getAllFamilies());
    }
}
