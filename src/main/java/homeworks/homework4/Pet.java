package homeworks.homework4;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    int trickLevel;// 1-100
    String[] habits;

    public void eat(){
        System.out.println("I'm eating");
    }
    public void respond(){
        System.out.println("Hello, owner. I am - "+this.nickname+". I miss you!");
    }
    public void foul(){
        System.out.println("I need to cover it up");
    }
    public Pet(){}
    public Pet(String species,String nickname){
        this.species=species;
        this.nickname=nickname;
    }
    public Pet(String species,String nickname,int age,int trickLevel,String[] habits){
        this(species,nickname);
        this.age=age;
        this.trickLevel=trickLevel;
        this.habits=habits;
    }
    @Override
    public String toString(){
        String show=this.species+"{ nickname = '"+this.nickname+"', age = "+this.age+", tricklevel = "+this.trickLevel+ ", habits = "+ Arrays.toString(this.habits) +" }";
        return show;
    }

}
