package homeworks.homework4;
import java.util.Arrays;
import java.util.Random;

public class Human {
    String name,surname;
    int year,iq;
    Human mother,father;
    Pet pet;
    String[][] schedule = new String[7][2];
    Random random=new Random();

    public void greetPet(){
        System.out.println("Hello, "+pet.nickname);
    }
    public void describePet(){
        System.out.println("I have a "+pet.species+", he is "+pet.age+" years old, he is "+(pet.trickLevel>50? "very sly": "almost not sly"));
    }
    public Human(){}
    public Human(String name, String surname,int year){
        this.name=name;
        this.surname=surname;
        this.year=year;
    }
    public Human(String name, String surname,int year,Human mother,Human father){
        this(name,surname,year);
        this.mother=mother;
        this.father=father;
    }
    public Human(String name, String surname,int year,int iq,Human mother,Human father,Pet pet,String[][] schedule){
        this(name,surname,year);
        this.iq=iq;
        this.mother=mother;
        this.father=father;
        this.pet=pet;
        this.schedule=schedule;
    }
    public boolean feedPet(boolean isTimeTofeed){
        if(isTimeTofeed || random.nextInt()<this.pet.trickLevel){
            System.out.println("Hm... I will feed "+this.pet.nickname+"");
            return true;
        }else{
            System.out.println("I think "+this.pet.nickname+" is not hungry.");
            return false;
        }
    }
    @Override
    public String toString(){
        String show;
        show="Human{ name='"+this.name+"', surname='"+this.surname+"', year="+this.year+", iq="+this.iq+", mother='"+this.mother.name+" "+this.mother.surname+"', father='"+this.father.name+" "+this.father.surname+"', ";
        show+="pet = "+this.pet.species+"{ nickname='"+this.pet.nickname+"', age="+this.pet.age+", tricklevel="+this.pet.trickLevel+", habits="+ Arrays.toString(this.pet.habits)+" }}";
        return show;
    }
}
