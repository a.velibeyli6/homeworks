package homeworks.homework6;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        String[][] schedule = {{DayOfWeek.Monday.name(), "go to the courses"}, {DayOfWeek.Tuesday.name(), "do homeworks"}, {DayOfWeek.Wednesday.name(), "p"}, {DayOfWeek.Thursday.name(), "go to the courses"}, {DayOfWeek.Friday.name(), "go to the courses"}, {DayOfWeek.Saturday.name(), "go to the courses"}, {DayOfWeek.Sunday.name(), "go to the courses"}};
        Human Phoebe = new Human("Phoebe", "Buffay", 1970, 75, schedule);
        Human Max = new Human("Max", "Buffay", 1971, 80, schedule);
        Human Joey = new Human("Joey", "Tribianni", 2000, 75, schedule);
        Human Rachel = new Human("Rachel", "Green", 2001, 78, schedule);
        Human Ross = new Human("Ross", "Geller", 1999, 85, schedule);
        String habits[] = {"playing", "sleeping", "woofing", "lying on the ground"};
        Pet pet = new Pet(Species.Dog, "pet", 5, 100, habits);
        Family friends = new Family(Phoebe, Max);
        friends.setPet(pet);
        friends.addChild(Joey);
        friends.addChild(Ross);
        friends.addChild(Rachel);


//        for cheking toString in the tests
//        ===========================================================
//        Human mother = new Human("Anna", "Karenina", 1980,90,schedule);
//        Human father = new Human("Lev", "Tolstoy", 1976,90,schedule);
//        Family friends = new Family(mother,father);
//        friends.setPet(pet);
//        Human child1 = new Human("Alexander", "Pushkin", 2000,80,schedule);
//        Human child2 = new Human("Mikhail", "Lermontov", 2001,81,schedule);
//        friends.addChild(child1);
//        friends.addChild(child2);
//        System.out.println(friends.toString());
//        System.out.println(child1);
//        System.out.println(pet);
//        =======================================================

        for(int i=0;i<10000000;i++){
            Human a = new Human();
        }
    }
}
