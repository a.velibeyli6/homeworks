package homeworks.homework6;
import java.lang.Override;
import java.util.Arrays;

public class Human {
    private String name, surname;
    private int year, iq;
    private Family family;
    private String[][] schedule = new String[7][2];

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return this.surname;
    }

    public String[][] getSchedule() {
        return this.schedule;
    }

    public int getYear() {
        return this.year;
    }

    public int getIq() {
        return this.iq;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////


    public Human() {
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, String[][] schedule) {
        this(name,surname,year);
        this.iq = iq;
        this.schedule = schedule;
    }
    @Override
    public boolean equals(Object o) {
        if(o==null) return false;
        if(!(o instanceof Human)) return false;
        Human that = (Human)o;
        if(this.year!=that.year) return false;
        if(!this.name.equals(that.name)) return false;
        if(!this.surname.equals(that.surname)) return false;
        return true;
    }

    @Override
    public void finalize(){
        System.out.println("I'm gonna die, wish me luck\nwith love "+name);
    }
    @Override
    public String toString() {
        String show;
        show = "Human{ name='" + this.name + "', surname='" + this.surname + "', year=" + this.year + ", iq=" + this.iq + ", schedule=" + Arrays.deepToString(this.schedule) + " }";
        return show;
    }
}
