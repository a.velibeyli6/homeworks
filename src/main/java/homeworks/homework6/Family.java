package homeworks.homework6;

import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private Pet pet;
    Random random = new Random();

    /////////////////////////////////////////////////////////////////////////////////////////////////////

    public Human getMother() {
        return this.mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return this.father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return this.children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Pet getPet() {
        return this.pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public Family(Human mother, Human father) {// ADDING FAMILY ONLY WHEN INITIALIZE IT.
        this.mother = mother;
        this.father = father;
    }

    public boolean addChild(Human child) {
        return children.add(child);

    }
    public Human deleteChild(int index) {
        return children.remove(index);
    }////////OVERRIDING DELETECHILD METHOD.

    public boolean deleteChild(Human o) {
        return children.remove(o);
    }

    public int countFamily() {
        return children.size() + 2;// Parents plus children;
    }

    public boolean feedPet(boolean timeTofeed) {
        if (timeTofeed || random.nextInt() < this.getPet().getTrickLevel()) {
            System.out.println("Hm... I will feed " + this.getPet().getNickname() + "");
            return true;
        } else {
            System.out.println("I think " + this.getPet().getNickname() + " is not hungry.");
            return false;
        }
    }

    public void greetPet() {
        System.out.println("Hello, " + this.getPet().getNickname());
    }

    public void describePet() {
        System.out.println("We have a " + this.getPet().getSpecies() + ", he is " + this.getPet().getAge() + " years old, he is " + (this.getPet().getTrickLevel() > 50 ? "very sly" : "almost not sly"));
    }
    @Override
    public void finalize() {
        System.out.println("I'm gonna die, wish me luck\nwith love <3 Family");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        StringBuilder show = new StringBuilder();
        show.append("Father ==> name='").append(this.father.getName()).append("', surname='").append(this.father.getSurname()).append("', year=").append(this.father.getYear()).append(", iq=").append(this.father.getIq()).append(", schedule= {").append(Arrays.deepToString(this.father.getSchedule())).append("\n");
        show.append("Mother ==> name='").append(this.mother.getName()).append("', surname='").append(this.mother.getSurname()).append("', year=").append(this.mother.getYear()).append(", iq=").append(this.mother.getIq()).append(", schedule= {").append(Arrays.deepToString(this.mother.getSchedule())).append("\n");
        for (Human child : children) {
            if (child != null) {
                show.append("Children ==> name='").append(child.getName()).append("', surname='").append(child.getSurname()).append("', year=").append(child.getYear()).append(", ");
                show.append("iq=").append(child.getIq()).append(", schudule=").append(Arrays.deepToString(child.getSchedule())).append("\n");
            }
        }
        show.append("Pet ==> nickname='").append(this.pet.getNickname()).append("', species='").append(this.pet.getSpecies()).append("', age=").append(this.pet.getAge()).append(", tricklevel=").append(this.pet.getTrickLevel()).append(", habits=").append(Arrays.toString(this.pet.getHabits())).append("\n");
        return show.toString();
    }


}