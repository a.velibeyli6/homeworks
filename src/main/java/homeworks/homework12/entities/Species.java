package homeworks.homework12.entities;

public enum Species {
    Dog,
    DomesticCat,
    RoboCat,
    Fish,
    Snake,
    Carrot,
    Mouse,
    Rat,
    Tortuga,
    UNKNOWN;
}
