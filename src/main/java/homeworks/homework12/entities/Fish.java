package homeworks.homework12.entities;

public class Fish extends Pet {

    public Fish(String nickname, int age, int trickLevel) {
        super(Species.Fish,nickname,age,trickLevel);
    }

    @Override
    public void respond() {
        System.out.println("Hello, owner. I am - " + getNickname() + ". I miss you!");
    }
}
