package homeworks.homework12;

public class MyException extends RuntimeException {

    private String errorCode = "ToManyChildrenException";
    public MyException(String message) {
        super(message);
    }

    public String getErrorCode() {
        return errorCode;
    }
}
