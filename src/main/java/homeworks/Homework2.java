package homeworks;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Homework2 {
    public static void output(){
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (i == 0)
                    System.out.print(j + " | ");
                else {
                    if (j == 0)
                        System.out.print(i + " | ");
                    else {
                        System.out.print("- | ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static boolean change(int[] aim,int[] shoot){
        boolean did=false;

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (i == 0)
                    System.out.print(j + " | ");
                else {
                    if (j == 0)
                        System.out.print(i + " | ");
                        //Work only here//////////////////////////////////////////////////////////////////////////////
                    else {
                        if(shoot[0] == aim[0] && shoot[1] == aim[1]){
                            did=true;
                        }
                        if(shoot[0]==i&&shoot[1]==j){
                            System.out.print("* | ");
                        }
                        else
                            System.out.print("- | ");
                    }
                }
            }
            System.out.println();
        }
        return did;
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        Random random = new Random();
        int[] aim = new int[2];
        for(int i=0;i<2;i++){
            aim[i]=random.nextInt(5)+1;
        }
        int[] shoot=new int[2];
        boolean did=false;
        output();
        while(did==false){
            System.out.print("please, enter a line (from 1 - to 5) : ");
            shoot[0]= in.nextInt();
            while(shoot[0]<=0 || shoot[0]>5){
                System.out.print("please, enter a line (from 1 - to 5) : ");
                shoot[0]= in.nextInt();
            }

            System.out.print("please, enter a column (from 1 - to 5) : ");
            shoot[1]=in.nextInt();
            while(shoot[1]<=0 || shoot[1]>5){
                System.out.print("please, enter a column (from 1 - to 5) : ");
                shoot[1]= in.nextInt();
            }
            did=change(aim,shoot);
        }
        System.out.println("congrats!, you won!");


    }
}
